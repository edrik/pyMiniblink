#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-08-06 10:12:01
# @Link         : https://gitee.com/edrik
# @Modified by  : Amber
# @Modified at  : 2019-08-21 15:03:34
# @Version      : v.0.1


class WkeError(Exception):
    """Base class for MiniBlink exceptions"""
    pass


class ScriptError(WkeError):
    """Raised by runtime from calling script when error occured"""

    def __init__(self, message, script=None):
        super().__init__(self, message.replace('\r', '\n'))
        self.message = message
        self.script = script

    def __repr__(self):
        return '%s("%s") at "%s"' % (type(self).__name__, self.message.replace(
            "\r", "\n").rstrip(), self.script if self.script else "<>")

    def __str__(self):
        return type(self).__name__ + ": " + self.message.replace("\r", "\n").rstrip()

    pass


class ScriptException(ScriptError):
    """Raised by script by throwing or returning Error instance."""

    def __init__(self, message, script=None):
        super().__init__(message, script)
        pass
