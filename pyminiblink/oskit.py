#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-08-06 10:15:34
# @Link         : https://gitee.com/edrik
# @Modified by  : Amber
# @Modified at  : 2019-08-21 15:03:29
# @Version      : v.0.1

import sys
import platform
import importlib

from datetime import datetime
from functools import wraps

OS, ARCH = sys.platform, platform.architecture()[0]
OS_WIN = OS == 'win32'
OS_LNX = OS == 'linux'
OS_OSX = OS == 'darwin'

ARCH_32 = ARCH == '32bit'
ARCH_64 = ARCH == '64bit'


def stop_watch(name='wke'):
    def wrap(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            begin = datetime.now().microsecond
            rs = fn(*args, **kwargs)
            end = datetime.now().microsecond
            print('%s spent time %dms' % (fn.__name__, end - begin))
            return rs

        return decorator

    return wrap


@stop_watch('pyminiblink')
def pretty_print(head, body):
    import prettytable as pt
    # ####################################################
    # see also:
    # PrettyPrinter
    # pprint
    # ####################################################
    tb = pt.PrettyTable()

    tb.field_names = head  # set head
    tb.sortby = head[1]
    for row in body:
        tb.add_row(row)
    # s = tb.get_string()
    tb.left_padding_width = 1
    tb.set_style(pt.PLAIN_COLUMNS)
    # tb.set_style(pt.RANDOM)
    # tb.set_style(pt.DEFAULT)
    # tb.set_style(pt.MSWORD_FRIENDLY)
    tb.align = 'l'
    tb.float_format = '2.2'
    tb.junction_char = '*'
    # tb.border = 1
    # tb.set_style(pt.DEFAULT)
    # tb.horizontal_char = '+'
    print(tb)


def module_diff(mod):
    keys = []
    for key in sys.modules.keys():
        keys.append(key)
    importlib.import_module(mod)
    for key in sys.modules.keys():
        if key not in keys:
            print(key, sys.modules[key])


def print_all_module():
    for key in sys.modules.keys():
        print(key, sys.modules[key])


if __name__ == '__main__':
    pretty_print('abc', ['ddd'])
