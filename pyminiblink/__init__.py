#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-08-06 10:08:07
# @Link         : https://gitee.com/edrik/pyminiblink
# @Modified by  : Amber
# @Modified at  : 2019-08-31 09:54:31
# @Version      : v.0.1

import os as _os
import sys as _sys

from . import context as ctx
from . import ext
from .capi.mbapi import api
from .window import Window

__author__ = 'Amber'
__name__ = 'pyMiniblink'
__version__ = (0, 0, 1)

wke = api

print(_os.name, _os.getcwd())
print('current encodig:', _sys.getdefaultencoding())
version = wke.wkeVersion()
print('vers.', version)
'''
__all__ = ['export']
def export(defn):
    fname = defn.__name__
    globals()[fname] = defn
    __all__.append(fname)
'''
