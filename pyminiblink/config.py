#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-08-06 10:17:44
# @Link         : https://gitee.com/edrik
# @Modified by  : Amber
# @Modified at  : 2019-08-23 11:24:41
# @Version      : v.0.1

import yaml


def load_config(filename='./miniblink.yaml'):
    try:
        return yaml.load(open(filename, 'rb'), Loader=yaml.FullLoader)
    except Exception as e:
        print(e)
        return {}


class Config:
    """docstring for Config"""
    DLL_ROOT = '/miniblink'
    DLL_NAME = 'node'
    DLL_NAME_X64 = 'node64'
    DLL_EXT = '.dll'
    DEBUG_PAGE = 'pages/inspect/inspector.html'
    PROJ_NAME = ''

    def __init__(self):
        pass


dic = load_config()
[setattr(Config, k, v) for k, v in dic.items()]
print('dll root:', Config.DLL_ROOT)
