#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-08-06 10:11:50
# @Link         : https://gitee.com/edrik
# @Modified by  : Amber
# @Modified at  : 2019-08-21 15:03:33
# @Version      : v.0.1


class EventHandler(object):
    """docstring for EventHandler"""

    def __init__(self):
        super(EventHandler, self).__init__()
