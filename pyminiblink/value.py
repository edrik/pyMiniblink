#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-08-06 10:12:14
# @Link         : https://gitee.com/edrik
# @Modified by  : Amber
# @Modified at  : 2019-08-21 15:03:26
# @Version      : v.0.1
'''
import enum


class VALUE_TYPE(enum.IntEnum):
    T_UNDEFINED = 0
    T_NULL = 1
    T_BOOL = 2
    T_INT = 3
    T_FLOAT = 4
    T_STRING = 5
    T_DATE = 6  # INT64 - contains a 64-bit value representing the number of 100-nanosecond intervals since January 1, 1601 (UTC), a.k.a. FILETIME on Windows
    T_CURRENCY = 7  # INT64 - 14.4 fixed number. E.g. dollars = int64 / 10000;
    T_LENGTH = 8  # length units, value is int or float, units are VALUE_UNIT_TYPE
    T_ARRAY = 9
    T_MAP = 10
    T_FUNCTION = 11
    T_BYTES = 12  # sequence of bytes - e.g. image data
    T_OBJECT = 13  # scripting object proxy (TISCRIPT/SCITER)
    T_DOM_OBJECT = 14  # DOM object (CSSS!), use get_object_data to get HELEMENT
    T_RESOURCE = 15  #
    T_RANGE = 16  # integer range N..M
    T_DURATION = 17  # time duration in seconds, stored as float
    T_ANGLE = 18  # angle value in radians, stored as float
    T_COLOR = 19  # color value, stored as 0xAABBGGRR integer


_python_types = {
    VALUE_TYPE.T_UNDEFINED: type(None),
    VALUE_TYPE.T_NULL: type(None),
    VALUE_TYPE.T_BOOL: bool,
    VALUE_TYPE.T_INT: int,
    VALUE_TYPE.T_FLOAT: float,
    VALUE_TYPE.T_STRING: str,
    VALUE_TYPE.T_ARRAY: list,
    VALUE_TYPE.T_MAP: dict,
    VALUE_TYPE.T_BYTES: bytes,
    VALUE_TYPE.T_DURATION: float,
    VALUE_TYPE.T_ANGLE: float,
    VALUE_TYPE.T_COLOR: int,
}

'''
