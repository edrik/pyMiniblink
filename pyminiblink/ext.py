#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-08-06 11:40:22
# @Link         : https://gitee.com/edrik
# @Modified by  : Amber
# @Modified at  : 2019-08-31 10:36:48
# @Version      : v.0.1

import logging
import inspect
from functools import wraps
from . import context as ctx

log = logging.getLogger(__name__)


def script(fname):
    '''
    @script
    @FN(VOID,PVOID...)
    def script_function():
        pass
    '''

    def wrap(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            return fn(*args, **kwargs)

        ctx.add_script(fname, fn)
        return decorator

    return wrap


def service(fn):
    '''Only module-function support
    Examples:
    @service
    def func(*args):
        print(args)
        return

    '''

    @wraps(fn)
    def decorator(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except Exception as e:
            print(e)
            return e, False

    log.debug('[service] fn type fn: %r method: %r class: %r', inspect.isfunction(fn), inspect.ismethod(fn),
              inspect.isclass(fn))
    # if inspect.isfunction(fn):
    #     raise serviceError('type[class-function] is not supported')
    fname = getattr(fn, '__name__')
    log.debug('[service] add backend-service function: %r', fname)
    ctx.add_ioc(fname, fn)
    return decorator
