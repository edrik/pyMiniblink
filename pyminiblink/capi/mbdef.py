#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-08-06 10:09:17
# @Link         : https://gitee.com/edrik
# @Modified by  : Amber
# @Modified at  : 2019-08-21 15:03:49
# @Version      : v.0.1

from enum import (auto, IntEnum)


class wkeNavigationType(IntEnum):
    WKE_NAVIGATION_TYPE_LINKCLICK = 1
    WKE_NAVIGATION_TYPE_FORMSUBMITTE = 1 << 1
    WKE_NAVIGATION_TYPE_BACKFORWARD = 1 << 2
    WKE_NAVIGATION_TYPE_RELOAD = 1 << 3

    # WKE_NAVIGATION_TYPE_LINKCLICK =1
    # WKE_NAVIGATION_TYPE_FORMSUBMITTE=2
    # WKE_NAVIGATION_TYPE_BACKFORWARD=3
    # WKE_NAVIGATION_TYPE_RELOAD=4
    WKE_NAVIGATION_TYPE_FORMRESUBMITT = 1 << 4
    WKE_NAVIGATION_TYPE_OTHER = 1 << 5


class wkeWindowType(IntEnum):
    WKE_WINDOW_TYPE_POPUP = 0
    WKE_WINDOW_TYPE_TRANSPARENT = 1
    WKE_WINDOW_TYPE_CONTROL = 2


class wkeMouseFlags(IntEnum):
    WKE_LBUTTON = 0x01,
    WKE_RBUTTON = 0x02,
    WKE_SHIFT = 0x04,
    WKE_CONTROL = 0x08,
    WKE_MBUTTON = 0x10,


class wkeKeyFlags(IntEnum):
    WKE_EXTENDED = 0x0100,
    WKE_REPEAT = 0x4000,


class wkeMouseMsg(IntEnum):
    WKE_MSG_MOUSEMOVE = 0x0200,
    WKE_MSG_LBUTTONDOWN = 0x0201,
    WKE_MSG_LBUTTONUP = 0x0202,
    WKE_MSG_LBUTTONDBLCLK = 0x0203,
    WKE_MSG_RBUTTONDOWN = 0x0204,
    WKE_MSG_RBUTTONUP = 0x0205,
    WKE_MSG_RBUTTONDBLCLK = 0x0206,
    WKE_MSG_MBUTTONDOWN = 0x0207,
    WKE_MSG_MBUTTONUP = 0x0208,
    WKE_MSG_MBUTTONDBLCLK = 0x0209,
    WKE_MSG_MOUSEWHEEL = 0x020A,


class WkeCursorInfoType(IntEnum):
    WkeCursorInfoPointer = auto()
    WkeCursorInfoCross = auto()
    WkeCursorInfoHand = auto()
    WkeCursorInfoIBeam = auto()
    WkeCursorInfoWait = auto()
    WkeCursorInfoHelp = auto()
    WkeCursorInfoEastResize = auto()
    WkeCursorInfoNorthResize = auto()
    WkeCursorInfoNorthEastResize = auto()
    WkeCursorInfoNorthWestResize = auto()
    WkeCursorInfoSouthResize = auto()
    WkeCursorInfoSouthEastResize = auto()
    WkeCursorInfoSouthWestResize = auto()
    WkeCursorInfoWestResize = auto()
    WkeCursorInfoNorthSouthResize = auto()
    WkeCursorInfoEastWestResize = auto()
    WkeCursorInfoNorthEastSouthWestResize = auto()
    WkeCursorInfoNorthWestSouthEastResize = auto()
    WkeCursorInfoColumnResize = auto()
    WkeCursorInfoRowResize = auto()
    WkeCursorInfoMiddlePanning = auto()
    WkeCursorInfoEastPanning = auto()
    WkeCursorInfoNorthPanning = auto()
    WkeCursorInfoNorthEastPanning = auto()
    WkeCursorInfoNorthWestPanning = auto()
    WkeCursorInfoSouthPanning = auto()
    WkeCursorInfoSouthEastPanning = auto()
    WkeCursorInfoSouthWestPanning = auto()
    WkeCursorInfoWestPanning = auto()
    WkeCursorInfoMove = auto()
    WkeCursorInfoVerticalText = auto()
    WkeCursorInfoCell = auto()
    WkeCursorInfoContextMenu = auto()
    WkeCursorInfoAlias = auto()
    WkeCursorInfoProgress = auto()
    WkeCursorInfoNoDrop = auto()
    WkeCursorInfoCopy = auto()
    WkeCursorInfoNone = auto()
    WkeCursorInfoNotAllowed = auto()
    WkeCursorInfoZoomIn = auto()
    WkeCursorInfoZoomOut = auto()
    WkeCursorInfoGrab = auto()
    WkeCursorInfoGrabbing = auto()
    WkeCursorInfoCusto = auto()


class wkeCookieCommand(IntEnum):
    wkeCookieCommandClearSessionCookies = auto()
    wkeCookieCommandFlushCookiesToFile = auto()
    wkeCookieCommandReloadCookiesFromFile = auto()


class CREATE_WINDOW_FLAGS(IntEnum):
    W_CHILD = (1 << 0)  # child window only, if this flag is set all other flags ignored
    W_TITLEBAR = (1 << 1)  # toplevel window, has titlebar
    W_RESIZEABLE = (1 << 2)  # has resizeable frame
    W_TOOL = (1 << 3)  # is tool window
    W_CONTROLS = (1 << 4)  # has minimize / maximize buttons
    W_GLASSY = (1 << 5)  # glassy window - supports "Acrylic" on Windows and "Vibrant" on MacOS.
    W_ALPHA = (1 << 6)  # transparent window ( e.g. WS_EX_LAYERED on Windows )
    W_MAIN = (1 << 7)  # main window of the app, will terminate the app on close
    W_POPUP = (1 << 8)  # the window is created as topmost window.
    W_ENABLE_DEBUG = (1 << 9)  # make this window inspector ready
    W_OWNS_VM = (1 << 10)  # it has its own script VM
