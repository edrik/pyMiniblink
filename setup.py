#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-07-25 16:16:20
# @Link         : https://gitee.com/edrik
# @Modified by  : Amber
# @Modified at  : 2019-08-23 10:30:33
# @Version      : v.0.1
# distutils, setuptools, distribute, distutils2
'''
try:
    from setuptools import setup
    # from setuptools import find_packages
except ImportError:
    from distutils.core import setup
'''
from setuptools import setup
# from setuptools import find_packages
setup(
    name='pyMiniblink',
    version="0.0.1",
    keywords=('pyMiniblink'),
    description='Python bindings for miniblink',
    license="MIT Licence",
    url="",
    author="Amber Lua",
    author_email="leyouyuan@126.com",
    # packages=find_packages(),
    packages=['pyminiblink', 'pyminiblink.capi'],
    include_package_data=True,
    platforms="win",
    install_requires=[],
    scripts=[],
    # entry_points={'console_scripts': ['test=test.help:main']},
    classifiers=[""],
    zip_safe=False,
)
