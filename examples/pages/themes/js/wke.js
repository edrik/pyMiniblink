/*
 * @Author: Abmer
 * @Date:   2019-08-06 16:05:29
 * @Last Modified by:   Amber
 * @Last Modified time: 2019-08-21 15:34:31
 */
const Wke = (function(window, undefiend) {
    // js_func_demo,
    // js_msgbox
    async function execute(fname, data) {
        if (fname === undefiend) {
            throw ArgumentError('fname')
        }
        var result = await wke_execute(fname, JSON.stringify(data))
        return result
    };

    function callback(fname) {
        var args = [].slice.call(arguments, 1);
        try {
            fn = eval(fname)
        } catch (e) {
            console.log(e)
        }
        if (typeof fn === 'function') {
            console.log('found native function:',fn)
            return fn.apply(this, args)
        }
    }

    return {
        execute: execute,
        callback: callback
    };
})(window);