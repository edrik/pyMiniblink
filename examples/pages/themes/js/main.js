const Main = (function(window, layui, undefined) {

    layui.use('tree', ()=>{
        let tree = layui.tree;
        let inst = tree.render({
            id: 'article_index',
            elem: '#article_index',
//             edit: true,
//             edit:['add','update','del'],
//             accordion: true,
            showLine: false,
            text: {
                defaultNodeName: ' - ',
                none: ' - '
            },
            click: function(node) {
                console.log(node.data, node)
            },
            operate: function(node) {
                var type = node.type
                  , data = node.data
                  , elem = node.elem;
                console.log(type, data, elem, node)

            },
            data: [{
                title: 'title1',
                id:1,

            }, {
                id: 'menu-id',
                title: 'title',
                href: '#',
                children: [{
                    title: 'title children'
                }]
            }]
        }); 
    }
    );

    return {};
}
)(window, layui);
