#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-07-25 14:46:41
# @Link         : https://gitee.com/edrik
# @Modified by  : Amber
# @Modified at  : 2019-08-21 16:03:17
# @Version      : v.0.1

from log import setup_logging
import logging
import sys
sys.path.append("..")
try:
    import pyminiblink as mb
except Exception as e:
    raise e

setup_logging()
log = logging.getLogger(__name__)

mb.config.Config.DLL_ROOT = '../dlls/miniblink'


class Frame(mb.Window):
    """docstring for Frame"""

    def __init__(self, *args, **kwargs):
        super(Frame, self).__init__(*args, **kwargs)


if __name__ == '__main__':
    log.info('pyminiblink - demo')
    frame = Frame(is_debug=True, size=(1600, 900))
    frame.load_file('pages/main.html')
    frame.mainloop()
