#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author       : Abmer
# @Date         : 2019-08-06 13:41:38
# @Link         : https://gitee.com/edrik
# @Modified by  : Amber
# @Modified at  : 2019-08-21 15:03:40
# @Version      : v.0.1
import yaml
import logging.config
import os

logging.Formatter.default_msec_format = '%s.%03d'


def setup_logging(default_path='log.yaml', default_level=logging.INFO, env_key="LOG_CFG"):
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'r') as f:
            config = yaml.load(f, Loader=yaml.FullLoader)
            logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


def func():
    logging.info('start func')

    logging.info('exec func')

    logging.info('end func')


if __name__ == '__main__':
    setup_logging(default_path='log.yaml')
    func()
